import numpy as np
import matplotlib.pyplot as plt

print('hello')

x = np.linspace(0,2*np.pi,num=100)
y = np.sin(x)

plt.plot(x,y,'b-')
plt.savefig('test.pdf',format='pdf')
